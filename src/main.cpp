#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>

//---------------------------------------------------------
// Constants
//---------------------------------------------------------
#define LED_PIN             25
#define NUM_PIXEL           72
#define PIXEL_FORMAT        NEO_GRB + NEO_KHZ800
#define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"
//---------------------------------------------------------
// Variables
//---------------------------------------------------------
Adafruit_NeoPixel *pixels_;
uint8_t update_flag_;
uint8_t color_[3];
Adafruit_NeoPixel *on_board_;
//---------------------------------------------------------
// Program
//---------------------------------------------------------
class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string value = pCharacteristic->getValue();

      if (value.length() > 0) {
        Serial.println("*********");
        Serial.print("New value: ");
        for (int i = 0; i < value.length(); i++) {
          color_[i] = static_cast<uint8_t>(value[i]);
          Serial.print(color_[i]);Serial.print(" ");
        }
        Serial.println();
        update_flag_ = 1;
      }
    }
};

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  BLEDevice::init("HeliIndicator");
  BLEServer *pServer = BLEDevice::createServer();

  BLEService *pService = pServer->createService(SERVICE_UUID);

  BLECharacteristic *pCharacteristic = pService->createCharacteristic(
                                         CHARACTERISTIC_UUID,
                                         BLECharacteristic::PROPERTY_READ |
                                         BLECharacteristic::PROPERTY_WRITE
                                       );

  pCharacteristic->setCallbacks(new MyCallbacks());

  pCharacteristic->setValue("This device is HeliIndicator");
  pService->start();

  BLEAdvertising *pAdvertising = pServer->getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
  pAdvertising->setMinPreferred(0x12);
  pAdvertising->start();
  // Prepare LED
  pixels_ = new Adafruit_NeoPixel(NUM_PIXEL, LED_PIN, PIXEL_FORMAT);
  pixels_->begin();
  update_flag_ = 1;
  for (uint8_t i = 0; i < 3; i++) {
    color_[i] = 0;
  }
  on_board_ = new Adafruit_NeoPixel(1, 27, PIXEL_FORMAT);
  on_board_->begin();
  on_board_->setPixelColor(0, on_board_->Color(127, 127, 127));
  on_board_->show();
}

void loop() {
  // put your main code here, to run repeatedly:
  if (update_flag_) {
    update_flag_ = 0;
    pixels_->clear();
    for(int i = 0; i < NUM_PIXEL; i++) {
      pixels_->setPixelColor(i, pixels_->Color(color_[0], color_[1], color_[2]));
    }
    pixels_->show();
  }
  delay(5);
}