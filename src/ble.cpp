#include "ble.h"
#include "packet.h"
#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

#define SERVICE_UUID        "55725ac1-066c-48b5-8700-2d9fb3603c5e"
#define CHARACTERISTIC_UUID "69ddb59c-d601-4ea4-ba83-44f679a670ba"
#define DEVICE_NAME         "HeliIndicator"

//---------------------------------------------------------
// Variables
//---------------------------------------------------------
BLEServer *pServer_ = NULL;
BLECharacteristic *pCharacteristic_ = NULL;
bool deviceConnected_ = false;
bool oldDeviceConnected_ = false;
std::string rxValue_;
std::string txValue_;
ManufactureData manufacture_data_;
//---------------------------------------------------------
// Callbacks
//---------------------------------------------------------
class MyServerCallbacks: public BLEServerCallbacks {
  void onConnect(BLEServer *pServer_) {
    deviceConnected_ = true;
    Serial.println("onConnect");
  };
  void onDisconnect(BLEServer *pServer_) {
    deviceConnected_ = false;
    Serial.println("onDisconnect");
  }
};

class MyCharacteristicCallbacks: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic_) {
    Serial.println("onWrite");
    std::string rxValue = pCharacteristic_->getValue();
    if( rxValue.length() > 0 ){
      Serial.print("Received Value: ");
      for(int i=0; i<rxValue.length(); i++ ){
        Serial.print(rxValue[i],HEX);
      }
      Serial.println();
    }
  }
};

//---------------------------------------------------------
// Functions
//---------------------------------------------------------
void InitBLE() {
    // Init
    BLEDevice::init(DEVICE_NAME);
    // Server
    pServer_ = BLEDevice::createServer();
    pServer_->setCallbacks(new MyServerCallbacks());
    // Service
    BLEService *pService = pServer_->createService(SERVICE_UUID);
    // Characteristic
    pCharacteristic_ = pService->createCharacteristic(
        CHARACTERISTIC_UUID,
        BLECharacteristic::PROPERTY_WRITE  |
        BLECharacteristic::PROPERTY_NOTIFY
    );
    pCharacteristic_->setCallbacks(new MyCharacteristicCallbacks());
    pCharacteristic_->addDescriptor(new BLE2902());
    //
    pService->start();
    // Advertising
    BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
    BLEAdvertisementData oAdvertisementData = BLEAdvertisementData();
    oAdvertisementData.setFlags(0x06);
    oAdvertisementData.addData(manufacture_data_.ToString());
    pAdvertising->setAdvertisementData(oAdvertisementData);
    pAdvertising->start();
    Serial.println("startAdvertising");
}

void ExcuteBLE() {
    // disconnecting
    if(!deviceConnected_ && oldDeviceConnected_){
        delay(500); // give the bluetooth stack the chance to get things ready
        pServer_->startAdvertising();
        Serial.println("restartAdvertising");
        oldDeviceConnected_ = deviceConnected_;
    }
    // connecting
    if(deviceConnected_ && !oldDeviceConnected_){
        oldDeviceConnected_ = deviceConnected_;
    }
}