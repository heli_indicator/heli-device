#include <stdint.h>
#include <string>
// https://github.com/OpenWonderLabs/SwitchBotAPI-BLE/blob/latest/devicetypes/ledstriplight.md


#define UUID 0x6909
#define RX_CHARACTERISTIC_UUID  "cba20002-224d-11e6-9fb8-0002a5d5c51b"
#define TX_CHARACTERISTIC_UUID  "cba20003-224d-11e6-9fb8-0002a5d5c51b"

#pragma pack(push, 1)
//---------------------------------------------------------
// Broadcast Packet
//---------------------------------------------------------
struct ManufactureData {
    uint8_t uuid[2];
    uint8_t mac_address[6];
    uint8_t sequence_number;
    uint8_t power_state_light_level;
    uint8_t delay_type_on_state_mode;
    uint8_t color[6];
    uint8_t fault_code;
    ManufactureData() : 
        uuid{0x09, 0x69},
        mac_address{0x01, 0x02, 0x03, 0x04, 0x05, 0x06} {}
    std::string ToString() {
        std::string data = "";
        // uuid
        for (int i = 0; i < 2; i++) {
            data += (char)uuid[i];
        }
        // mac address
        for (int i = 0; i < 6; i++) {
            data += (char)mac_address[i];
        }
        // sequence_number
        data += (char)sequence_number;
        // power_state_light_level
        data += (char)power_state_light_level;
        // delay_type_on_state_mode
        data += (char)delay_type_on_state_mode;
        // color
        for (int i = 0; i < 6; i++) {
            data += (char)color[i];
        }
        // fault_code
        data += (char)fault_code;
        // add length
        data = (char)data.length() + data;
        return data;
    }
};

//---------------------------------------------------------
// Request Packet
//---------------------------------------------------------
enum RequestPacketCommand {
    TURN_ON = 0x01,
    TURN_OFF = 0x02,
    TOGGLE = 0x03,
    CHANGE_ALL = 0x12,
    CHANGE_LEVEL = 0x14,
    CHANGE_RGB = 0x16
};

struct ReqPacketHeader {
    uint8_t magic_number;
    uint8_t header;
    ReqPacketHeader() : 
        magic_number(0x57),
        header(0x0F) {}
};

struct ReqPacketChangeStatus {
    ReqPacketHeader header;
    uint8_t reserve0;
    uint8_t reserve1;
    uint8_t cmd;
    ReqPacketChangeStatus() : 
        header(ReqPacketHeader()),
        reserve0(0x49),
        reserve1(0x01),
        cmd(RequestPacketCommand::TURN_ON) {}
};

struct ReqPacketChangeAll {
    ReqPacketHeader header;
    uint8_t reserve0;
    uint8_t reserve1;
    uint8_t cmd;
    uint8_t level;
    uint8_t rgb[3];
    ReqPacketChangeAll() : 
        header(ReqPacketHeader()),
        reserve0(0x49),
        reserve1(0x01),
        cmd(RequestPacketCommand::CHANGE_ALL),
        level(0),
        rgb{0, 0, 0} {}
};

struct ReqPacketChangeLevel {
    ReqPacketHeader header;
    uint8_t reserve0;
    uint8_t reserve1;
    uint8_t cmd;
    uint8_t level;
    ReqPacketChangeLevel() : 
        header(ReqPacketHeader()),
        reserve0(0x49),
        reserve1(0x01),
        cmd(RequestPacketCommand::CHANGE_LEVEL),
        level(0) {}
};

struct ReqPacketChangeRGB {
    ReqPacketHeader header;
    uint8_t reserve0;
    uint8_t reserve1;
    uint8_t cmd;
    uint8_t rgb[3];
    ReqPacketChangeRGB() : 
        header(ReqPacketHeader()),
        reserve0(0x49),
        reserve1(0x01),
        cmd(RequestPacketCommand::CHANGE_RGB),
        rgb{0, 0, 0} {}
};

struct ReqPacketReadStatus {
    ReqPacketHeader header;
    uint8_t reserve0;
    uint8_t reserve1;
    ReqPacketReadStatus() : 
        header(ReqPacketHeader()),
        reserve0(0x4A),
        reserve1(0x01) {}
};

//---------------------------------------------------------
// Response Packet
//---------------------------------------------------------
enum ResponseMode {
    COLOR = 0x02,
    SCENE = 0x03,
    MUSIC = 0x04
};

struct ResPacketHeader{
    uint8_t reserve0;
    uint8_t power_status;
    uint8_t level;
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t reserve1[3];
    uint8_t reserve2;
    uint8_t mode;
    ResPacketHeader() : 
        reserve0(0x01),
        power_status(0),
        level(0),
        r(0),
        g(0),
        b(0),
        reserve2(0xFF),
        mode(ResponseMode::COLOR) {}
};

#pragma pack(pop)